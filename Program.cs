﻿using System;
using System.Linq;

namespace RotateArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int[] n = new int[]{1,2,3,4,5,6,7};
            
            for (int i = 0; i < n.Length; i++)
            {
                System.Console.Write(n[i] + " ");
            }System.Console.WriteLine();
            Rotate(n, 3);
            for (int i = 0; i < n.Length; i++)
            {
                System.Console.Write(n[i] + " ");
            }
        }


        public static void Rotate(int[] nums, int k){
            k %= nums.Length;
            Reverse(nums, 0, nums.Length - 1);
            Reverse(nums, 0, k - 1);
            Reverse(nums, k, nums.Length - 1);
            
        }

        public static void Reverse(int[] nums, int start, int end){
            while(start < end){
                int temp = nums[start];
                nums[start] = nums[end];
                nums[end] = temp;
                start++;
                end--;
            }
        }

        // BRUTE FORCE
        // public static void Rotate(int[] nums, int k) 
        // {
        //     int temp, previous;
        //     for (int i = 0; i < k; i++)
        //     {
        //         previous = nums[nums.Length - 1];
        //         for (int j = 0; j < nums.Length; j++)
        //         {
        //             temp = nums[j];
        //             nums[j] = previous;
        //             previous = temp;
        //         }
        //     }
        // }
    }
}
